#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError 
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  print("Setting up host parameters...")
  print("\n")
  mastodon_hostname = input("Enter Mastodon hostname: ")
  weather_db = input("Weather db name: ")
  weather_db_user = input("Weather db user: ")
  owm_api_key = input("OpenWeatherMap API key (get yours at https://openweathermap.org/api): ")
  default_country = input("Default country where to search if none are provided by user (Ex. UK or US or IT or FR ...): ")
  html_dir = input("Full path of the root dir of your serving html host (with trailing slash): ")
  domain = input("Domain or subdomain where you will serve generated html. (Ex. https://niceweathermaps.net): ")
  time_zone = input("Time zone of your Mastodon server (Ex. Europe/Brussels): ")
  with open(file_path, "w") as text_file:
    print("mastodon_hostname: {}".format(mastodon_hostname), file=text_file)
    print("weather_db: {}".format(weather_db), file=text_file)
    print("weather_db_user: {}".format(weather_db_user), file=text_file)
    print("owm_api_key: {}".format(owm_api_key), file=text_file)
    print("default_country: {}".format(default_country), file=text_file)
    print("html_dir: {}".format(html_dir), file=text_file)
    print("domain: {}".format(domain), file=text_file)
    print("time_zone: {}".format(time_zone), file=text_file)

def create_table(db, db_user, table, sql):

  try:

    conn = None
    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()


    print("Creating table.. "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################
  
# Load configuration from config file
config_filepath = "config.txt"
weather_db = get_parameter("weather_db", config_filepath) # E.g., weather_prod
weather_db_user = get_parameter("weather_db_user", config_filepath) # E.g., mastodon

############################################################
# create database
############################################################

try:

  conn = psycopg2.connect(dbname='postgres',
      user=weather_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  print("Creating database " + weather_db + ". Please wait...")
  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(weather_db))
      )
  print("Database " + weather_db + " created!")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = weather_db, user = weather_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("config.txt")
  print("Exiting. Run setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("Host parameters saved to config.txt!")
  print("\n")

############################################################
# Create needed tables 
############################################################

print("Creating table...")

########################################

db = weather_db
db_user = weather_db_user
table = "weather"
sql = "create table "+table+" (datetime timestamptz, id bigint, query_user varchar(40), answer boolean, notif_type varchar(12), status_created_at timestamptz, attended_total int, ambiguous_total int, nonexistent_total int,"
sql += "ignored_total int, queries_total int, query varchar(100), perc_attended numeric(5,2), perc_ambiguous numeric(5,2), perc_nonexistent numeric(5,2), perc_ignored numeric(5,2))"

create_table(db, db_user, table, sql)

#####################################

print("Done!")
print("Now you can run setup.py!")
print("\n")
