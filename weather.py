#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
from six.moves import urllib
from datetime import datetime, timedelta
import pytz
from subprocess import call
from mastodon import Mastodon
import unidecode
import time
import threading
import re
import os
import json
import time
import signal
import sys
import os.path        # For checking whether secrets file exists
import requests       # For doing the web stuff, dummy!
import operator      
import calendar
import psycopg2
import pyowm
from pyowm import timeutils
from datetime import timedelta, datetime
from pyowm import OWM
from pyowm.tiles.enums import MapLayerEnum
from pyowm.utils.geo import Point
from pyowm.commons.tile import Tile
import urllib.request
import folium
from folium.map import *
from folium import plugins
from folium.plugins import MeasureControl
from folium.plugins import FloatImage
import geopy
from geopy.geocoders import Nominatim

from decimal import *
getcontext().prec = 2

###############################################################################
# INITIALISATION
###############################################################################

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):    
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)
weather_db = get_parameter("weather_db", config_filepath)
weather_db_user = get_parameter("weather_db_user", config_filepath)
owm_api_key = get_parameter("owm_api_key", config_filepath)
default_country = get_parameter("default_country", config_filepath) 
html_dir = get_parameter("html_dir", config_filepath)
domain = get_parameter("domain", config_filepath)
time_zone = get_parameter("time_zone", config_filepath)

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname, 
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

#############################################################

remaining_requests = mastodon.ratelimit_remaining
print("API remaining requests: " + str(remaining_requests))

proxim_reset = mastodon.ratelimit_reset
proxim_reset = datetime.fromtimestamp(proxim_reset)
proxim_reset = proxim_reset.strftime("%d/%m/%Y, %H:%M:%S")
print("Next reset at: " + str(proxim_reset))

############################################################

tz = pytz.timezone(time_zone)
now = datetime.now(tz)

owm = pyowm.OWM(owm_api_key)

attended = 0
ambiguous = 0
nonexistent = 0
ignored = 0
queries = 0

perc_attended = 0.00
perc_ambiguous = 0.00
perc_ignored = 0.00
perc_nonexistent = 0.00

###################################################################
# search creation data of the last already answered query
###################################################################

try:

  conn = None
  conn = psycopg2.connect(database = weather_db, user = weather_db_user, password = "", host = "/var/run/postgresql", port = "5432")

  cur = conn.cursor()

  cur.execute("select status_created_at from weather order by id desc limit 1")

  row = cur.fetchone()

  if row is not None:
    last_query = row[0]
  else:
    last_query = ''

  cur.close()

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################################################
# check for new notifications by comparing creation datetime with the last answered one of the database
#############################################################################################################################

if last_query != '':

  try:

    created = mastodon.notifications()[0]['created_at']

  except Exception as error:
  
    sys.exit(error)

  creation = created + timedelta(hours=2)
  creation = creation.strftime("%d/%m/%Y, %H:%M:%S")

  last_notification = last_query.strftime("%d/%m/%Y, %H:%M:%S")

  if creation == last_notification:

    print("No new notifications!")
    sys.exit(0)

###################################################################
# query the totals of the last database row
###################################################################

try:

  conn = None
  conn = psycopg2.connect(database = weather_db, user = weather_db_user, password = "", host = "/var/run/postgresql", port = "5432")

  cur = conn.cursor()

  cur.execute("select attended_total, ambiguous_total, nonexistent_total, ignored_total, queries_total from weather order by datetime desc limit 1")

  row = cur.fetchone()
  if row != None:
    attended = row[0]
    ambiguous = row[1]
    nonexistent = row[2]
    ignored = row[3]
    queries = row[4]

  cur.close()

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

####################################################################

pending = mastodon.notifications()

i = 0
while i < len(pending):

  ignore = False
  notif_type =  mastodon.notifications()[i]['type']
  if notif_type == 'reblog':
    i += 1
    continue
  if notif_type == 'favourite':
    i += 1
    continue
  if notif_type == 'follow':
    i += 1
    continue
  if notif_type == 'mention':
    status_id = mastodon.notifications()[i]['status']['id']

  try:

    conn = None
    conn = psycopg2.connect(database = weather_db, user = weather_db_user, password = "", host = "/var/run/postgresql", port = "5432")

    cur = conn.cursor()

    ### check if already answered

    cur.execute("SELECT id, query_user, answer FROM weather where id=(%s)", (status_id,))

    row = cur.fetchone()

    if row == None:

      if notif_type == 'mention':

        user_id = mastodon.notifications()[i].account.id
        account = mastodon.notifications()[i]['account']['acct']
        id =  mastodon.notifications()[i]['id']
        created = mastodon.notifications()[i]['created_at']
        content = mastodon.notifications()[i]['status']['content']
        visibility = mastodon.notifications()[i].status.visibility

        content = cleanhtml(content)

        ###########################################################   

        start = content.index("@")
        end = content.index(" ")
        if len(content) > end:

          content = content[0: start:] + content[end +1::]

        clean = content.count('@')

        i = 0
        while i < clean :

          start = content.rfind("@")
          end = len(content)
          content = content[0: start:] + content[end +1::]
          i += 1

        question = content
        question = question.lstrip(" ")

        ############################################################

        user = account

        if notif_type == 'mention':

          if unidecode.unidecode(question)[0:8] == "weather:":
            
            if question.find(',') != -1:
              country = question.rsplit(',', 1)[1]
              country = country.replace(' ', '')
              country = country.upper()
            else:

              country = default_country 

            start = 0
            end = unidecode.unidecode(question).index('weather:',0)

            question = question.split(',')[0]
            
            if len(question) > end :

              question = question[0: start:] + question[end +9::]

            try:

              now = datetime.now()

              reg = owm.city_id_registry()
              tune_up = reg.ids_for(question, country)

              if len(tune_up) == 0:

                nom = Nominatim()
                n=nom.geocode(question)
                print(n.latitude, n.longitude)
                #obs = owm.weather_around_coords(n.latitude, n.longitude, limit=4)
                obs = owm.weather_at_coords(n.latitude, n. longitude)
                zoom = 16  
              elif len(tune_up) == 1:
                locations = reg.locations_for(question, country)
                obs = owm.weather_at_id(tune_up[0][0])
                zoom = 14
              elif len(tune_up) > 1:
                listing = [item for item in tune_up if item[2] == country]
                obs = owm.weather_at_id(listing[0][0])
                zoom = 14

              location = obs.get_location()
              long = location.get_lon()
              lat  = location.get_lat()

              weather = obs.get_weather()
              weather_rebut = weather.get_reference_time(timeformat='iso')
 
              icon = weather.get_weather_icon_name()
              icon_url = weather.get_weather_icon_url()

              detailed = weather.get_detailed_status()
              temperature = weather.get_temperature('celsius')['temp']
              wind = weather.get_wind()
              if wind.get('gust') != None:
                wind_gust = wind['gust']
              else:
                wind_gust = "-"
              if wind.get('deg') != None:
                wind_deg = wind['deg']
              else:
                wind_deg = "-"
              if wind.get('speed') != None:
                wind_speed = wind['speed']
              else:
                wind_speed = "-"
              humidity = weather.get_humidity()
              pressure = weather.get_pressure()['press']
              sunrise = weather.get_sunrise_time(timeformat='iso')
              sunset = weather.get_sunset_time(timeformat='iso')

              #forecast = owm.three_hours_forecast(question)
              #prev = forecast.get_forecast()
              #on = prev.get_location() 

              #zoom = 10

              geopoint = Point(long, lat)
              x_tile, y_tile = Tile.tile_coords_for_point(geopoint, zoom)
              layer_name = MapLayerEnum.PRECIPITATION
              tm = owm.tile_manager(layer_name)
              tile = tm.get_tile(x_tile, y_tile, zoom)
              tile.persist(str(question)+'.png')  
              
              m = folium.Map(location=[lat,long], tiles="Openstreetmap", zoom_start=zoom)
              folium.TileLayer(tiles='Stamen Toner',name="Stamen Toner").add_to(m)
              folium.TileLayer(tiles='Stamen Terrain',name="Stamen Terrain").add_to(m)
              folium.TileLayer(tiles='CartoDB positron',name="CartoDB dark_matter").add_to(m)

              folium.map.Marker([lat,long], popup=str(question)+": "+str(detailed)+"\n"+"Temp: " + str(temperature)+"º", tooltip=str(question)+"\n"+str(temperature)+"º", draggable=False).add_to(m)

              folium.LayerControl().add_to(m)
              link = question
              link = link.replace(" ", "")
              m.save(html_dir + link + ".html")

              weathererror = 0            

            except ValueError as verror:

              print("ValueError: Country must be a 2-char string")
              tempserror = 1
              notif_type = "nonexistent"
              answered = True
              nonexistent += 1
              queries += 1
              perc_attended = (attended * 100.00) / queries
              perc_ambiguous = (ambiguous * 100.00) / queries
              perc_ignored = (ignored * 100.00) / queries
              perc_nonexistent = (nonexistent * 100.00) / queries
              toot_text = "@"+account+ " " +"\n"
              toot_text += "Country must be a 2-char string \n"
              toot_text += "Ex.: \n"
              toot_text += "@weather query: New York, US"
              mastodon.status_post(toot_text, in_reply_to_id=id_estat,visibility=visibilitat)

            except:

              weathererror = 1
              if len(tune_up) == 0:
                print("Can't find " + question)
              else: 
                print("API error?")
              notif_type = "nonexistent"
              answered = True
              nonexistent += 1
              queries += 1
              perc_attended = (attended * 100.00) / queries
              perc_ambiguous = (ambiguous * 100.00) / queries
              perc_ignored = (ignored * 100.00) / queries
              perc_nonexistent = (nonexistent * 100.00) / queries
              toot_text = "@"+account+ " " +"\n"
              toot_text += "Can't find it, sorry!\n"
              toot_text += "Please, try another city near to " + question 
 
            if weathererror == 0:

              toot_text = "@"+account+ " " +"\n"
              toot_text += "The weather at " + question + " is:" + "\n"
              toot_text +=  ":"+icon+": "+ str(detailed) + " \n"
              toot_text +=  "\n"
              toot_text += "- Latitude " + str(lat) + " Longitude " + str(long) + "\n"
              toot_text += "- Wind gust " + str(wind_gust) + " m/s\n"
              toot_text += "- Wind direction " + str(wind_deg) + "º\n"
              toot_text += "- Wind speed " + str(wind_speed) + " m/s\n" 
              toot_text += "- Temperature " + str(temperature) + "º C" "\n"
              toot_text += "- Humidity " + str(humidity) + " %\n"
              toot_text += "- Pressure " + str(pressure) + " hpa\n"
              toot_text += "- Sunrise time " + str(sunrise) + "\n"
              toot_text	+= "- Sunset time " + str(sunset) + "\n"
              toot_text += "Interactive map: " + domain + "/" + link +".html" + "\n" 
              toot_text += "Reception time : " + str(weather_rebut)

              notif_type = "attended"
              answered = True
              attended += 1
              queries += 1
              perc_attended = (attended * 100.00) / queries
              perc_ambiguous = (ambiguous * 100.00) / queries
              perc_ignored = (ignored * 100.00) / queries
              perc_nonexistent = (nonexistent * 100.00) / queries

          else: ## user query don't have "weather:"

            ignore = True
            notif_type = "ignored"
            answered = True
            ignored += 1
            queries += 1
            perc_attended = (attended * 100.00) / queries
            perc_ambiguous = (ambiguous * 100.00) / queries
            perc_ignored = (ignored * 100.00) / queries
            perc_nonexistent = (nonexistent * 100.00) / queries
            question = (question[:90] + '... ') if len(question) > 90 else question
 
          if answered == True:

            if ignore == False:

              mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)
              print("Tooting")

            ################################################################################################################################
            # save to database just answered user query

            insert_line = """INSERT INTO weather(datetime, id, query_user, answer, notif_type, status_created_at, attended_total, ambiguous_total, nonexistent_total, ignored_total, queries_total, 
                                                          query, perc_attended, perc_ambiguous, perc_nonexistent, perc_ignored) 
                                                          VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""

            conn = None

            try: 

              conn = psycopg2.connect(database = weather_db, user = weather_db_user, password = "", host = "/var/run/postgresql", port = "5432")

              cur = conn.cursor()

              cur.execute(insert_line, (now, status_id, account, answered, notif_type, created, attended, ambiguous, nonexistent, ignored, queries, question, perc_attended, perc_ambiguous, perc_nonexistent, perc_ignored))

              conn.commit()

              cur.close()

            except (Exception, psycopg2.DatabaseError) as error:

              print(error)

            finally:

              if conn is not None:

                conn.close()

            ################################################################################################################################
 
            i += 1 
     
          else:

            break

    else:

      sys.exit(0)

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)
    sys.exit(":-(")

  finally:

    if conn is not None:

      conn.close()

