# mastoweather

Weather Bot for Mastodon, written in Python.

Any user can ask the bot for the weather of any country like this: @bot weather: New York  
The bot will reply/toot with all the information given by OpenWeatherMap API.  
To run this bot you need to get your API key at https://openweathermap.org/api

### Dependencies

-   **Python 3**
-   Postgresql server
-   OpenWeatherMap API key
-   Everything else at the top of `fediverse.py`!

### Usage:

Within Python Virtual Environment:

1. Run 'db-setup.py' to create needed database and table and the rest of parameters. All collected data will be written there. You can use software like Grafana to visualize your bot metrics!

2. Run 'setup.py' to get your Mastodon's access token of an existing user. It will be saved to 'secrets/secrets.txt' for further use.

3. Run 'weather.py' to start your bot and wait for users's weather queries. It will toot OpenWeatherMap information with a link of generated map of the queried country.  

Note: install all needed packages with 'pip install package'
